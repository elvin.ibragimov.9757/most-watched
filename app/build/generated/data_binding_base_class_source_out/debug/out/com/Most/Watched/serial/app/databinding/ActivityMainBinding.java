// Generated by view binder compiler. Do not edit!
package com.Most.Watched.serial.app.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.Most.Watched.serial.app.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityMainBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final Button exitBtn;

  @NonNull
  public final Button tvShowsBtn;

  private ActivityMainBinding(@NonNull ConstraintLayout rootView, @NonNull Button exitBtn,
      @NonNull Button tvShowsBtn) {
    this.rootView = rootView;
    this.exitBtn = exitBtn;
    this.tvShowsBtn = tvShowsBtn;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_main, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityMainBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.exitBtn;
      Button exitBtn = ViewBindings.findChildViewById(rootView, id);
      if (exitBtn == null) {
        break missingId;
      }

      id = R.id.tvShowsBtn;
      Button tvShowsBtn = ViewBindings.findChildViewById(rootView, id);
      if (tvShowsBtn == null) {
        break missingId;
      }

      return new ActivityMainBinding((ConstraintLayout) rootView, exitBtn, tvShowsBtn);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
